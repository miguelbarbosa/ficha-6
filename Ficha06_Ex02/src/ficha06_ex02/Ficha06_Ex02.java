/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficha06_ex02;
import java.util.Scanner;
/**
 *
 * @author 1181339
 */
public class Ficha06_Ex02 {
    
    static Scanner scanner = new Scanner (System.in);
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        double num1, num2;
        String operador; 
        
        System.out.print("Escreva o primeiro numero: ");
        num1 = scanner.nextDouble();
        
        do {   
            System.out.println("Escolha um operador (+, -, x, %): ");
            operador = scanner.next();  
        } while (operador != "+" || operador != "-" || operador != "x" || operador != "%" );
        
        System.out.print("Escreva o segundo numero: ");
        num2 = scanner.nextDouble();
    }
    
}
